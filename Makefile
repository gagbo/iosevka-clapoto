# SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>
#
# SPDX-License-Identifier: MIT

SHELL = /usr/bin/env bash
JOBS ?= 8
FONT_VERSION ?= 16.3.4
HEADLESS ?= 0

ifeq ($(HEADLESS),0)
	interactive = -t
	docker_jobs = --jCmd=$(JOBS)
else
	interactive =
	docker_jobs =
endif

.PHONY: install build term patch patch-term build-term clean all

all: classic term

clean:
	rm -rf dist dist-NF

classic: build patch

term: build-term patch-term

build: private-build-plans.toml
	mkdir -p dist
	docker run --rm -i $(interactive) \
		-v $$(pwd):/build \
		-e FONT_VERSION=$(FONT_VERSION) \
		--cpus=$(JOBS) \
		avivace/iosevka-build \
		contents::iosevka-clapoto $(docker_jobs)

build-term: private-build-plans.toml
	mkdir -p dist
	docker run --rm -i $(interactive) \
		-v $$(pwd):/build \
		-e FONT_VERSION=$(FONT_VERSION) \
		--cpus=$(JOBS) \
		avivace/iosevka-build \
		contents::iosevka-clapoto-term $(docker_jobs)

patch:
	mkdir -p dist-NF/iosevka-clapoto-nf/ttf
	docker run --rm -i $(interactive) \
		-v $$(pwd)/dist/iosevka-clapoto/ttf:/in -v $$(pwd)/dist-NF/iosevka-clapoto-nf/ttf:/out \
		nerdfonts/patcher \
		--adjust-line-height --complete --progressbars

patch-term:
	mkdir -p dist-NF/iosevka-clapoto-term-nf/ttf
	docker run --rm -i $(interactive) \
		-v $$(pwd)/dist/iosevka-clapoto-term/ttf:/in -v $$(pwd)/dist-NF/iosevka-clapoto-term-nf/ttf:/out \
		nerdfonts/patcher \
		--adjust-line-height --complete --progressbars

install:
	mkdir -p ~/.local/share/fonts
	cp -r dist/* ~/.local/share/fonts
	chown $${USER}:$${USER} -R ~/.local/share/fonts
	fc-cache -f -v

install-nf:
	mkdir -p ~/.local/share/fonts
	cp -r dist-NF/* ~/.local/share/fonts
	chown $${USER}:$${USER} -R ~/.local/share/fonts
	fc-cache -f -v
