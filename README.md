<!--
SPDX-FileCopyrightText: 2022 Gerry Agbobada <git@gagbo.net>

SPDX-License-Identifier: CC0-1.0
-->

# Iosevka Clapoto

This is a customized build of iosevka for my personal usage.
Clone at your own risk. I will not give any reasons for the
changes that will happen.

Thanks to [Prot](https://git.sr.ht/~protesilaos/iosevka-comfy)
for inspiring me and convincing that I can too, make the font
that I like

## Build

### The lazy way
All necessary steps can be done by using `make && make install`, which
will install the font locally on Linux only for the current user.

It basically just uses the docker image to build the font with some
lazily hardcoded arguments that I like, and then install the fonts
in the correct location to be picked up by a `fc-cache` invocation.

### More control
Use the docker build image:
```bash
docker run -it -v $(pwd):/build avivace/iosevka-build
```
and you can add the flags you want, like the font version of Iosevka to use,
or the number of CPUs, or the variants to build etc.

If you do not want to use the docker image, you are probably proficient
enough not to need that readme.

## Private build documentation

The main repo for Iosevka has a nice
[wiki](https://github.com/be5invis/Iosevka/blob/master/doc/custom-build.md)
page.
